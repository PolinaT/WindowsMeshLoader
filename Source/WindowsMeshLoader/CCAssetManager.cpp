// Fill out your copyright notice in the Description page of Project Settings.

#include "CCAssetManager.h"


UCCAssetManager::UCCAssetManager()
{
 
}

bool UCCAssetManager::saveToFile(const TArray<uint8> &Data, const FString& Bucket,const FString& FileName, const FString& FileExtension)
{
    if (Data.Num() <= 0) {
        return false;
    }

    const FString& Path = FPaths::ProjectContentDir() / "HandheldARBP/Models" / *Bucket / *FileName + "." + *FileExtension;
    
    if (FFileHelper::SaveArrayToFile(Data, *Path))
    {
        return true;
    }

    return false;

}

bool UCCAssetManager::decodeBase64ToData(const FString& Base64Data, TArray<uint8> &Data) 
{
	return FBase64::Decode(Base64Data, Data);
}



UStaticMesh * UCCAssetManager::LoadAssetFromFile(const FString& Bucket, const FString& FileName, const FString& MeshName)
{
    const FString& Path = TEXT("/Game/HandheldARBP/Models/") + Bucket + TEXT("/") + FileName + "." + MeshName;
    return LoadObject<UStaticMesh>(NULL, *Path, NULL, LOAD_None, NULL);
    
    //return NULL;//Cast<UStaticMesh>object;
}


