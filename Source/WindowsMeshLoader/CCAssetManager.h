// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Engine.h"
#include "Core.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "CCAssetManager.generated.h"
/**
 * 
 */
UCLASS(BlueprintType, Blueprintable)
class WINDOWSMESHLOADER_API UCCAssetManager : public UObject
{
	GENERATED_BODY()

public:
    UCCAssetManager(); // Constructor
    
	UFUNCTION(BlueprintCallable, Category = "CCAssetManager")
    bool saveToFile(const TArray<uint8> &Data, const FString& Bucket,const FString& FileName, const FString& FileExtension);

	UFUNCTION(BlueprintCallable, Category = "CCAssetManager")
	bool decodeBase64ToData(const FString& Base64Data, TArray<uint8> &Data);

    UFUNCTION(BlueprintCallable, Category = "CCAssetManager")
    UStaticMesh * LoadAssetFromFile(const FString& Bucket, const FString& FileName, const FString& MeshName);
};
