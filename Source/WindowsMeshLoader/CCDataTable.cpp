// Fill out your copyright notice in the Description page of Project Settings.

#include "CCDataTable.h"
#include "FCCARItem.h"
#include "Engine/DataTable.h"

UCCDataTable::UCCDataTable()
{

}

bool UCCDataTable::AddNewRow(UDataTable *DataTable, FCCARItem ARItem) const
{
    DataTable->AddRow(FName(*ARItem.Name), ARItem);
    return true;
}

bool UCCDataTable::GetRow(UDataTable* DataTable, FName ARItemName) const
{
    FCCARItem* Row = DataTable->FindRow<FCCARItem>(ARItemName, FString("ARItem"));
    return Row ? true : false;
}

bool UCCDataTable::UpdateRow(UDataTable* DataTable, FCCARItem ARItem) const
{
    if(this->GetRow(DataTable, FName(*ARItem.Name)))
    {
        return false;
    }
    else
    {
        DataTable->AddRow(FName(*ARItem.Name), ARItem);
        return true;
    }
}

bool UCCDataTable::DeleteRow(UDataTable* DataTable, FName ARItemName) const
{
    DataTable->RemoveRow(ARItemName);
    return true;
}
